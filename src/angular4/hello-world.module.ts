/**
 * Created by jlong on 5/2/17.
 */
import 'zone.js';
import {NgModule}      from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {MyHelloWorldClass}  from './hello-world.component.ts';

@NgModule({
    imports:      [ BrowserModule ],
    declarations: [ MyHelloWorldClass ],
    bootstrap:    [ MyHelloWorldClass ]
})
export class PluginModule { }