/**
 * Created by jlong on 5/9/17.
 */
import Template from './tpl.html!text';
import Ctrl from './controller.js';

import LOCALE_EN from './l10n/en.json!';
import LOCALE_DE from './l10n/de.json!';
import LOCALE_ZH from './l10n/zh-Hans.json!';

export function Locales() {
    return {
        'en': LOCALE_EN,
        'de': LOCALE_DE,
        'zh-Hans': LOCALE_ZH
    };
}

export function BuildViews() {
    return {
        partialViews: {
            "frameworks-demo-tile-view-unique-id": {
                template: Template,
                controller: Ctrl
            }
        }
    };
}

export function Metadata() {
    return {
        dashboardTitle: "other-frameworks.title"
    };
}