# README #

### What is this repository for? ###

This repository demonstrates third-party module development with Angular 2/4 and React for the KUKA Connect platform.

### Who do I talk to? ###

@jlong4096

## Installation ##

```sh
$ curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
$ sudo apt-get install -y nodejs

$ sudo npm -g install jspm@beta
$ jspm init
```

## Development ##

Use the [KC UI Dev Kit](https://bitbucket.org/kukaaustin/kcui-dev-kit) to follow existing examples, develop, and visualize your new module.  You could copy the contents of your third-party folder into /src/ of this repository.

## Release ##

### Add Babel support for JSX and/or Angular 2 decorators ###

```sh
$ jspm install --dev npm:babel-plugin-transform-decorators-legacy npm:babel-plugin-transform-react-jsx
```

Then update jspm.config.js packages.<module_name>.main.meta:

```json
"*.ts": {
  "loader": "plugin-babel",
    "babelOptions": {
      "plugins": ["babel-plugin-transform-decorators-legacy"]
    }
  },
"*.jsx": {
  "loader": "plugin-babel",
    "babelOptions": {
      "plugins": ["babel-plugin-transform-react-jsx"]
    }
  }
```

Install any dependencies you want bundled with your module:

```sh
$ jspm install json text
```

All other dependencies should be identified as externals to the JSPM build and marked as peerDependencies in package.json.  Angular, Angular 2/4, and React are good candidates for external dependencies.  This keeps your module small.

### Build your release package ###

```sh
$ jspm build src/module.js dist/output.js --format cjs --skip-source-maps --minify --externals react react-dom @angular/core @angular/platform-browser @angular/platform-browser-dynamic reflect-metadata zone.js
```

This builds a single JS modules with all your included dependencies (including libraries, assets, HTML templates, JSON data, etc...).  The resulting file will have been statically optimized via Rollup, minified, and mangled.

### Update your package.json ###

```json
{
  "name": "<package name>",
  "version": "0.0.1",
  "registry": "npm",
  "jspmPackage": true,
  "main": "dist/<output>.js",
  "jspm": {
    "name": "frameworks-example",
    "main": "dist/output.js",
    ...
    "peerDependencies": {
      "react": "npm:react@^15.5.4",
      "react-dom": "npm:react-dom@^15.5.4",
      "@angular/core": "npm:@angular/core@^4.1.1",
      "@angular/platform-browser": "npm:@angular/platform-browser@4.1.1",
      "@angular/platform-browser-dynamic": "npm:@angular/platform-browser-dynamic@^4.1.1",
      "reflect-metadata": "npm:reflect-metadata@^0.1.10",
      "zone.js": "npm:zone.js@^0.8.10"
    },
    ...
  }
}
```

### Tagging ###

```sh
$ git commit -a
$ git tag v0.0.1
$ git push
$ git push origin v0.0.1
```
